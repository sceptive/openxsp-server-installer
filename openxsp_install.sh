#!/usr/bin/env ksh
CL_GREEN='\033[0;32m'
CL_LGREEN='\033[1;32m'
CL_NC='\033[0m'
CL_RED='\033[0;31m'
CL_LCYAN='\033[1;36m'
CL_LRED='\033[1;31m'
CL_CYAN='\033[0;36m'
CL_WHITE='\033[1;37m'
CL_PURPLE='\033[0;35m'
CL_LBLUE='\033[1;34m'
CL_YEL='\033[1;33m'

HOSTNAME=`hostname`

if [[ `whoami` != "root" ]]; then
	echo "Having user(s) with sudo right is a bad practice for security. Run this script as root."
	exit 1
fi

node -v > /dev/null 2>&1 || ( echo "${CL_GREEN}Installing nodeJS...${CL_NC}" && export PKG_PATH=https://ftp.openbsd.org/%m && pkg_add "node-8.9.4" )
node -v > /dev/null 2>&1 || ( echo "${CL_RED}Could not install nodeJS.${CL_NC}" && exit 1 )

cat > script.js << SCRPT
	// Consts
	C_VERSION = "0.0.1";
	C_TMP_DIR = "/tmp";
	C_TMP_LIBZMQ_PATH = C_TMP_DIR+"/libzmq";
	C_TIMEZONE_FILE = "/usr/share/zoneinfo/Etc/UTC";
	C_ORIGINAL_LOCALTIME_FILE = "/etc/localtime";
	C_BACKUP_LOCALTIME_FILE = "/etc/localtime.backup";
	C_TIMEZONE_SYNC_POOL = "pool.ntp.org";
	C_PACKAGES = ["mariadb-server-10.0.34v1", "nginx-1.12.2", "cmake-3.10.2", "boost-1.66.0", "libevent-2.0.22p0", "miniupnpc-1.9p1", "libldns-1.7.0", "gtest-1.8.0p2", "autoconf-2.68p1","automake-1.15.1", "libtool-2.4.2p0" ];
	C_NODE_PACKAGES = ["npm-check-updates", "pm2"];
	C_ENVIRONMENT_VARS = { "PKG_PATH": "https://ftp.openbsd.org/%m", 
					"AUTOCONF_VERSION": "2.68", 
					"AUTOMAKE_VERSION" : "1.15",
					"CXXFLAGS" : "-I/usr/local/include",
					"CFLAGS" : "-I/usr/local/include",
					"LDFLAGS" : "-L/usr/local/lib",
					"CC" : "clang",
					"CXX" : "clang++"
				 };
	C_PASSWORD_LEN = 32;
	C_LIBZMQ_REPOS = "https://bitbucket.org/sceptive/openxsp-libzmq.git";
	C_POOLJS_POOL_REPOS = "https://bitbucket.org/sceptive/openxsp-nodejs-pool.git";
	C_POOLUI_REPOS = "https://bitbucket.org/sceptive/openxsp-poolui.git";
	C_SESSION_FILE = ".session.openxsp";
	
	C_POOL_USER = "xsp_pool";
	C_POOL_USER_HOME = "/home/"+C_POOL_USER;
	C_POOL_APP_PATH = C_POOL_USER_HOME+"/app";

	C_NGINX_USER = "xsp_www";
	C_NGINX_USER_HOME = "/home/"+C_NGINX_USER;
	C_NGINX_LOGS_PATH = C_NGINX_USER_HOME+"/logs";
	C_NGINX_WWW_PATH = C_NGINX_USER_HOME+"/www";
	C_NGINX_CONFIG_TEMPLATE_PATH = "nginx.conf.template";

	C_POOLJS_POOL_PATH = C_POOL_APP_PATH+"/pooljs/";
	C_POOLJS_POOL_CFG_EXAMPLE_PATH = C_POOLJS_POOL_PATH+"/config_example.json";
	C_POOLJS_POOL_CFG_PATH = C_POOLJS_POOL_PATH+"/config.json";
	C_POOLJS_BASESQL_EXAMPLE_PATH = C_POOLJS_POOL_PATH+"/deployment/base.sql";
	C_POOLJS_BASESQL_TMP_PATH = C_TMP_DIR+"/pooljs.base.sql";
	
	C_POOLUI_PATH = C_POOL_APP_PATH+"/poolui/";
	C_POOLDB_PATH = C_POOL_APP_PATH+"/pooldb/";


	// Session vars
	SESSION_MYSQL_PASSWORD = null;
	SESSION_POOL_USER_PASSWORD = null;
	SESSION_POOL_AUTHKEY = null;
	SESSION_POOL_SECKEY = null;
	SESSION_NGINX_USER_PASSWORD = null;
	SESSION_MYSQL_POOL_PASSWORD = null;

	const rl = require('readline');	
	const fs = require('fs');
	const crypto = require('crypto');
	const spawnSync = require('child_process').spawnSync;
	const STAT = function(description) {
		console.log("${CL_LCYAN}[${CL_WHITE}+${CL_LCYAN}] ${CL_CYAN}"+description+"${CL_NC}");
	}
	const SUCC = function(description) {
		console.log("${CL_LGREEN}[${CL_WHITE}+${CL_LGREEN}] ${CL_GREEN}"+description+"${CL_NC}");
	}
	const FAIL = function(description) {
		console.log("${CL_LRED}[${CL_WHITE}+${CL_LRED}] ${CL_RED}"+description+"${CL_NC}");
	}
	const has_output = function(command,args,expecting) {
		out = spawnSync(command,args, { env: C_ENVIRONMENT_VARS } );
		out.output.forEach(function(item) {
			if (item != null && item.indexOf(expecting) != -1) {
				return true;
			}
		});
		if (out.stdout != null && out.stdout.indexOf(expecting) != -1) {
			return true;
		}
		return false;
	}
	const has_package = function(package) {
		return has_output("pkg_info",[],package);
	}
	const exec_sync = function(command,args,description,quit_on_fail,on_data) {
		STAT(description+"...")
		ret = spawnSync(command,args, { env: C_ENVIRONMENT_VARS });
		if (quit_on_fail && (ret.error != null || ret.status != 0)) {
			FAIL("Houston.. we have a problem !");
			console.log("STDOUT: \\n${CL_GREEN}"+ret.stdout+"${CL_NC}");
			console.log("STDERR: \\n${CL_RED}"+ret.stderr+"${CL_NC}");
			process.exit(1);
		}	
		return (ret.error == null);
	}
	const delete_if_exists = function (file_path,description) {
		if (is_exists(file_path,false)) {
			exec_sync("rm",["-rf",file_path],"Removing "+description);
		} 
	}
	const is_exists = function(file_path, quit_on_fail) {
		ret = fs.existsSync(file_path);
		if (!ret && quit_on_fail) {
			FAIL("${CL_WHITE}"+file_path+"${CL_RED} could not found.");
			process.exit(1);
		}

		return ret;
	}
	const write_file = function (file_path, content, quit_on_fail) {
		try{
			fs.writeFileSync(file_path, content); 
			return true;
		} catch (e) { 
			FAIL("Failed to write file ${CL_WHITE}"+file_path+"${CL_RED}");
			if (quit_on_fail) {
				process.exit(1);
			}
		} 	
		return false;
	}
	const gen_password = function () {
		ret = "";
		chspace = "_+-/@!$%^&*()[]}{ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		rbuf = crypto.randomBytes(C_PASSWORD_LEN+1)
		while (ret.length < C_PASSWORD_LEN+1) {
			ret += chspace.charAt(rbuf[ret.length] % chspace.length);
		}
		return ret;
	}
	const first_match = function(content,regex) {
		match = regex.exec(content)
		if (match.length < 1) {
			return null;
		}
		return match[1];
	}

	// Print banner

	console.log("");
	console.log("${CL_YEL}         _.---.${CL_LBLUE} ▄████▄                     ▀████  ▐███▀ ▄██████   ▄██████▄${CL_NC}");
	console.log("${CL_YEL}        ';-..--${CL_LBLUE}███  ███                      ███▌ ███▀ ██    ██  ███   ███${CL_NC}");
	console.log("${CL_YEL}         '}====${CL_LBLUE}███  ███  ▄███▄  ▄███ █   █    ███▐██   ██        ███   ███${CL_NC}");
	console.log("${CL_YEL}        .:     ${CL_LBLUE}███  ███ ███ ██ ██  ████  ██   ▀████▀   ██        ███   ███${CL_NC}");
	console.log("${CL_YEL}      _;:_     ${CL_LBLUE}███  ███ ███ ██ ██   ████ ██   █████  ▀█████████▀▀███████▀${CL_NC}");
	console.log("${CL_YEL}    ;'--.-';   ${CL_LBLUE}███  ███▀█████▀▀██▀▀ ██ █ ██  ▐███▀██         ██  ███${CL_NC}");
	console.log("${CL_YEL}    }====={    ${CL_LBLUE}███  ███ ███    ██  ███ ████ ▄███   ██        ██ ▀███${CL_NC}");
	console.log("${CL_YEL}  .'    _  '.  ${CL_LBLUE} ▀████▀ ▄████   █████ █ ▀███████    ███▄███████▀ ▄███▀${CL_NC}");
    console.log("${CL_YEL} /${CL_WHITE}::${CL_YEL}         \\\\^.^        /_;:_        \\\\ ${CL_WHITE}Server Installer Script ${CL_CYAN}v"+C_VERSION);
	console.log("${CL_YEL}|${CL_WHITE}::${CL_YEL}           |:_     _-;^--.-^;       |");
	console.log("${CL_YEL}\\\\${CL_WHITE}::. ${CL_YEL}         /  ^^^^^  }====={        /");
	console.log("${CL_YEL} ^${CL_WHITE}::_${CL_YEL}      _.^        .^    _  ^.   _.^");
	console.log("${CL_YEL}     ^^^^^^          /${CL_WHITE}::${CL_YEL}         \\\\^^");
	console.log("${CL_YEL}                    |${CL_WHITE}::${CL_YEL}           |");
	console.log("${CL_YEL}                    \\\\${CL_WHITE}::. ${CL_YEL}         /");
	console.log("${CL_YEL}                     ^${CL_WHITE}::_${CL_YEL}      _.^");
	console.log("${CL_YEL}                         ^^^^^^");
	console.log("");
	
	SESSION_MYSQL_PASSWORD = gen_password();
	SESSION_POOL_USER_PASSWORD = gen_password();
	SESSION_NGINX_USER_PASSWORD = gen_password();
	SESSION_MYSQL_POOL_PASSWORD = gen_password();
	SESSION_POOL_AUTHKEY = gen_password();
	SESSION_POOL_SECKEY = gen_password();
	write_file(C_SESSION_FILE,"mysqlp="+SESSION_MYSQL_PASSWORD+"\\n"
					+"poolup="+SESSION_POOL_USER_PASSWORD+"\\n"
					+"nginxup="+SESSION_NGINX_USER_PASSWORD+"\\n"
					+"mpoolp="+SESSION_MYSQL_POOL_PASSWORD+"\\n"
					+"authkey="+SESSION_POOL_AUTHKEY+"\\n"
					+"seckey="+SESSION_POOL_SECKEY+"\\n"
					,true);





	SESSION_DIR = process.cwd();

	function inst_clean_up() {

		if (has_output("cat",["/etc/passwd"],C_POOL_USER+":")) {
			exec_sync("ksh",["-c","rmuser "+C_POOL_USER+" <<!\\ny\\ny\\n!"],"Removing existing "+C_POOL_USER+" user",true);
		}
		if (has_output("cat",["/etc/passwd"],C_NGINX_USER+":")) {
			exec_sync("ksh",["-c","rmuser "+C_NGINX_USER+" <<!\\ny\\ny\\n!"],"Removing existing "+C_NGINX_USER+" user",true);
		}
		if (has_package("mariadb-server")) {
			STAT("Removing existing MySQL installation");
			exec_sync("/etc/rc.d/mysqld",["stop"], "Stopping MySQL instance", true);
			exec_sync("pkg_delete",["-I","-Ddependencies","mariadb-server","mariadb-client"], "Uninstalling packages", true);
		}

		delete_if_exists(C_POOL_USER_HOME,"XSP Pool Home");
		delete_if_exists(C_NGINX_USER_HOME,"XSP WWW Home");
		delete_if_exists(C_ORIGINAL_LOCALTIME_FILE,"localtime file");
		delete_if_exists(C_NGINX_LOGS_PATH," existing NGINX logs");
		delete_if_exists(C_POOL_APP_PATH," existing app directory");
		delete_if_exists(C_TMP_LIBZMQ_PATH," existing LIBZMQ build path");
		delete_if_exists("/var/mysql/"," MySQL datasets");





	}
	
	function inst_update_timezone() {
		STAT("Updating timezone to UTC..");
		is_exists(C_TIMEZONE_FILE, true);
		exec_sync("ln",["-sf", C_TIMEZONE_FILE, C_ORIGINAL_LOCALTIME_FILE], "Changing to UTC time-zone", true);
		exec_sync("rdate",["-nv",C_TIMEZONE_SYNC_POOL],"Synchronizing time with "+C_TIMEZONE_SYNC_POOL,true);
	}


	function inst_add_packages() {
		C_PACKAGES.forEach( function (package) {
			if (!has_package(package)) {
				exec_sync("pkg_add",["-x", "-aa", package], "Installing "+package, true);
			} else {
				SUCC(package+" already installed");
			}
		});
	}


	function inst_launch_mysqld() {
		exec_sync("rcctl",["enable","mysqld"],"Enabling MySQL Daemon", true);
		exec_sync("/usr/local/bin/mysql_install_db",[],"Installing MySQL system tables", true);
		exec_sync("/etc/rc.d/mysqld",["start"],"Starting MySQL daemon",true);
		exec_sync("/usr/local/bin/mysqladmin",["-u","root","password",SESSION_MYSQL_PASSWORD],"Setting MySQL root password",true);
		write_file("/root/.my.cnf", "[client]\\nuser=root\\npassword="+SESSION_MYSQL_PASSWORD+"\\n", true);
	}

	function inst_add_npm_packages() {
		C_NODE_PACKAGES.forEach( function(n_package) {
			exec_sync("npm",["install", n_package,"-g"],"Installing NodeJS package "+n_package,true);
		});
	}		

	function inst_libzmq() {
		is_exists(C_TMP_DIR, true);
		process.chdir(C_TMP_DIR);
		exec_sync("git",["clone",C_LIBZMQ_REPOS,C_TMP_LIBZMQ_PATH], "Clonning "+C_LIBZMQ_REPOS, true);
		process.chdir(C_TMP_LIBZMQ_PATH);
		exec_sync("./autogen.sh",[], "Building LibZMQ (1/3)", true);
		exec_sync("./configure",[],"Building LibZMQ (2/3)", true);
		exec_sync("gmake",["-j","4"], "Building LibZMQ (3/3)", true);
		exec_sync("gmake",["install"], "Installing LibZMQ", true);
		process.chdir(SESSION_DIR);
		exec_sync("rm",["-rf","/tmp/libzmq"], "Removing LibZMQ build dir", true);
	}

	function inst_add_user() {
		exec_sync("adduser",["-batch", C_POOL_USER, "nogroup", "XSP POOL User", SESSION_POOL_USER_PASSWORD, "-shell", "ksh"],"Adding "+C_POOL_USER+" user", true);
		
	}

	function inst_pooljs() {
		exec_sync("su",[C_POOL_USER,"-c", "mkdir "+C_POOL_APP_PATH], "Creating APP dir", true);
		exec_sync("su",[C_POOL_USER,"-c", "cd "+C_POOL_USER_HOME+" && git clone "+C_POOLJS_POOL_REPOS+" "+C_POOLJS_POOL_PATH], "Clonning NodeJS-Pool",true);
		exec_sync("su",[C_POOL_USER,"-c", "cd "+C_POOLJS_POOL_PATH+" && npm install"], "Installing NodeJS-Pool packages", true);
		exec_sync("su",[C_POOL_USER,"-c", "cd "+C_POOLJS_POOL_PATH+" && openssl req -subj '/C=IT/ST=Pool/L=Daemon/O=OpenXSP Pool/CN=openxsp.pool' "
												+"-newkey rsa:2048 -nodes -keyout cert.key -x509 -out cert.pem -days 36500"],
												"Generating pool certificate", true);
		exec_sync("su",[C_POOL_USER,"-c", "mkdir "+C_POOLDB_PATH], "Creating pooldb dir", true);
		nodejs_config = fs.readFileSync(C_POOLJS_POOL_CFG_EXAMPLE_PATH,{ encoding : 'utf-8'});
		nodejs_config = nodejs_config.replace("testpool.com","${HOSTNAME}");
		nodejs_config = nodejs_config.replace("CHANGEME",C_POOLDB_PATH);
		nodejs_config = nodejs_config.replace(/98erhfiuehw987fh23d/g,SESSION_MYSQL_POOL_PASSWORD);
		write_file(C_POOLJS_POOL_CFG_PATH,nodejs_config,true);
		exec_sync("chown",[C_POOL_USER+":"+C_POOL_USER, C_POOLJS_POOL_CFG_PATH], "Setting ownership", true);

		pooljs_sql = fs.readFileSync(C_POOLJS_BASESQL_EXAMPLE_PATH,{encoding: 'utf-8'});
		pooljs_sql = pooljs_sql.replace(/98erhfiuehw987fh23d/g,SESSION_MYSQL_POOL_PASSWORD);
		// BugFix on MariaDB "Index column size too large." error
		pooljs_sql = pooljs_sql.replace(/varchar\\(256\\)/g,"varchar(255)");
		pooljs_sql += ("INSERT INTO pool.config (module, item, item_value, item_type, Item_desc) "
						+"VALUES ('api', 'secKey','"+SESSION_POOL_SECKEY+"', 'string', 'HMAC key "
						+"for Passwords.  JWT Secret Key. Changing this will invalidate all current"
						+" logins.');\\n");
		pooljs_sql += ("INSERT INTO pool.config (module, item, item_value, item_type, Item_desc) "
						+"VALUES ('api', 'authKey','"+SESSION_POOL_AUTHKEY+"', 'string', 'Auth key "
						+"sent with all Websocket frames for validation. ');\\n");
		write_file(C_POOLJS_BASESQL_TMP_PATH, pooljs_sql);
		exec_sync("/bin/ksh",["-c","mysql -u root --password=\\""+SESSION_MYSQL_PASSWORD+"\\" < "+C_POOLJS_BASESQL_TMP_PATH],
			"Creating NodeJS-Pool tables in MySQL", true);
	}

	function inst_poolui() {
		exec_sync("su",[C_POOL_USER,"-c", "cd "+C_POOL_USER_HOME+" && git clone "+C_POOLUI_REPOS+" "+C_POOLUI_PATH], "Clonning PoolUI",true);
		exec_sync("su",[C_POOL_USER,"-c", "cd "+C_POOLUI_PATH+" && npm install"], "Installing PoolUI packages",true);
		exec_sync("su",[C_POOL_USER,"-c", "cd "+C_POOLUI_PATH+" && ./node_modules/bower/bin/bower update"], "Updating Bower",true);
		exec_sync("su",[C_POOL_USER,"-c", "cd "+C_POOLUI_PATH+" && ./node_modules/gulp/bin/gulp.js build"], "Building with gulp",true);
		exec_sync("/bin/ksh",["-cc","cp -r "+C_POOLUI_PATH+"/build/* "+C_NGINX_WWW_PATH+"/"], "Copying built poolui to "+C_NGINX_USER+" home", true);
		exec_sync("chown",["-R",C_NGINX_USER+":wheel",C_NGINX_WWW_PATH], "Setting access rights", true);
		exec_sync("/etc/rc.d/nginx",["start"], "Reloading NGINX instance", true);
	}

	function inst_configure_and_launch_nginx() {
		exec_sync("rcctl",["enable","nginx"],"Enabling NGINX Daemon", true);
		exec_sync("adduser",["-batch", C_NGINX_USER, "nogroup", "XSP NGINX User", SESSION_NGINX_USER_PASSWORD, "-shell", "nologin"],"Adding "+C_NGINX_USER+" user", true);
		exec_sync("mkdir",[C_NGINX_LOGS_PATH], "Creating LOGS dir", true);
		exec_sync("mkdir",[C_NGINX_WWW_PATH], "Creating WWW dir", true);
		exec_sync("chown",["-R",C_NGINX_USER+":wheel",C_NGINX_LOGS_PATH], "Setting access rights (1/2)", true);
		exec_sync("chown",["-R",C_NGINX_USER+":wheel",C_NGINX_WWW_PATH], "Setting access rights (2/2)", true);

		nginx_config = fs.readFileSync(C_NGINX_CONFIG_TEMPLATE_PATH,{ encoding : 'utf-8'});
		nginx_config = nginx_config.replace(/_C_NGINX_USER_/g,C_NGINX_USER);
		nginx_config = nginx_config.replace(/_C_NGINX_LOGS_PATH_/g,C_NGINX_LOGS_PATH);
		nginx_config = nginx_config.replace(/_C_NGINX_USERHOME_/g,C_NGINX_USER_HOME);
		nginx_config = nginx_config.replace(/_C_NGINX_WWW_PATH_/g,C_NGINX_WWW_PATH);
		write_file("/etc/nginx/nginx.conf", nginx_config);

		
		rc_conf_local = fs.readFileSync("/etc/rc.conf.local",{ encoding : 'utf-8'});
		rc_conf_local = rc_conf_local.replace(/^nginx_flags.*$/g,'');
		rc_conf_local += "\\nginx_flags=-u\\n";
		write_file("/etc/rc.conf.local");
		
		exec_sync("/usr/local/sbin/nginx",["-t"],"Testing NGINX configuration", true);
		exec_sync("/etc/rc.d/nginx",["start"], "Starting NGINX instance", true);
	}

	function launch_installation() {
		inst_clean_up();
		inst_update_timezone();
		inst_add_packages();
		inst_launch_mysqld();
		inst_configure_and_launch_nginx();
		inst_add_npm_packages();
		inst_libzmq();
		inst_add_user();
		inst_pooljs();
		inst_poolui();
		SUCC("Installation completed successfully");
	}

	const rl_int = rl.createInterface({
		input: process.stdin,
		output: process.stdout
	});

	rl_int.question('${CL_WHITE}This script will ${CL_YEL}remove${CL_WHITE} existing installations of MySQL / MariaDB and other packages  with stored datas. \\nIn case of any active usage of the OpenBSD it is not recommended to continue.\\nThis script is just for fresh installations. Are you sure to continue (y/n) ? ', (answer) => {

		if (answer == 'y' || answer == 'Y') {
			launch_installation();
		}
	  	rl_int.close();
 	});



SCRPT

node script.js 
rm script.js

